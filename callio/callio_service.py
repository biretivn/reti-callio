from typing import Union, Optional

from compose import compose

from callio.pipeline.interface import Pipeline
from db.bigquery import load, update


def pipeline_service(
    pipeline: Pipeline,
    start: Optional[str],
    end: Optional[str],
) -> dict[str, Union[str, int]]:
    return compose(
        lambda x: {
            "table": pipeline.name,
            "start": start,
            "end": end,
            "output_rows": x,
        },
        load(
            pipeline.name,
            pipeline.schema,
            pipeline.partition_key,
            update(pipeline.id_key, pipeline.cursor_key, pipeline.partition_key),
        ),
        pipeline.transform,
        pipeline.get,
        pipeline.params_fn(pipeline.name, pipeline.cursor_key),
    )((start, end))
