from typing import Any, Callable, Union, Optional
import os
import asyncio
from datetime import datetime
from urllib.parse import urljoin

import httpx

from db.bigquery import get_last_timestamp

PAGE_SIZE = 500
BASE_URL = "https://clientapi.phonenet.io/"
ID_PER_PAGE = 15


def _dynamic_params_fn(
    from_key: str,
    to_key: str,
    params: Optional[dict[str, Any]] = {},
):
    def _fn(table: str, cursor_key: str):
        def __fn(
            timeframe: tuple[Optional[str], Optional[str]]
        ) -> dict[str, Union[str, int]]:
            _start, _end = timeframe
            start = (
                datetime.strptime(_start, "%Y-%m-%d")
                if _start
                else get_last_timestamp(table, cursor_key)
            )
            end = datetime.strptime(_end, "%Y-%m-%d") if _end else datetime.utcnow()
            return {
                **params,  # type: ignore
                from_key: int(start.timestamp() * 1000),
                to_key: int(end.timestamp() * 1000),
            }

        return __fn

    return _fn


static_params_fn = lambda *args: lambda *args: None
create_params_fn = lambda p={}: _dynamic_params_fn("from", "to", p)
update_params_fn = lambda p={}: _dynamic_params_fn("fromUpdateTime", "toUpdateTime", p)


def get_client() -> httpx.AsyncClient:
    return httpx.AsyncClient(
        headers={"token": os.getenv("CALLIO_TOKEN", "")},
        timeout=None,
        limits=httpx.Limits(max_keepalive_connections=10, max_connections=10),
    )


async def get_one(
    client: httpx.AsyncClient,
    uri: str,
    params: dict[str, Any],
    callback_fn: Callable[[dict[str, Any]], Any],
    page: int = 1,
    method: str = "GET",
    body: Optional[dict[str, Any]] = None,
):
    r = await client.request(
        method,
        urljoin(BASE_URL, uri),
        params={**params, "pageSize": PAGE_SIZE, "page": page},
        json=body,
    )
    r.raise_for_status()
    res = r.json()
    return callback_fn(res)


GetListing = Callable[[dict[str, Any]], list[dict[str, Any]]]


def get_listing(uri: str) -> GetListing:
    def _get(params: dict[str, Any]) -> list[dict[str, Any]]:
        async def __get():
            async with get_client() as client:
                pages = await get_one(client, uri, params, lambda x: x["totalPages"])
                tasks = [
                    asyncio.create_task(
                        get_one(client, uri, params, lambda x: x["docs"], page)
                    )
                    for page in range(1, pages + 1)
                ]
                return await asyncio.gather(*tasks)

        return [i for j in asyncio.run(__get()) for i in j]

    return _get


def get_static(
    get_listing_fn: GetListing,
    params: list[dict[str, Any]],
    uri: str,
    id_fn: Callable[[dict[str, Any]], str] = lambda x: x["_id"],
    body_fn: Callable[[list[str]], dict[str, Any]] = lambda ids: {"ids": ids},
):
    def _get(*args) -> list[dict[str, Any]]:
        async def __get(ids_groups: list[list[str]]):

            async with get_client() as client:
                tasks = [
                    asyncio.create_task(
                        get_one(
                            client,
                            uri,
                            {},
                            lambda x: x,
                            method="POST",
                            body=body_fn(id_group),
                        )
                    )
                    for id_group in ids_groups
                ]
                return await asyncio.gather(*tasks)

        ids = [id_fn(i) for param in params for i in get_listing_fn(param)]
        ids_grouped = [
            ids[i : i + ID_PER_PAGE] for i in range(0, len(ids), ID_PER_PAGE)
        ]

        return [i for j in asyncio.run(__get(ids_grouped)) for i in j]

    return _get
