from callio.pipeline import pipelines
from callio import callio_service


def callio_controller(body: dict[str, str]):
    return callio_service.pipeline_service(
        pipelines[body.get("table", "")],
        body.get("start"),
        body.get("end"),
    )
